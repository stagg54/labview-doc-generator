---
title: About this project
subtitle: AntiDoc is the cure to your LabVIEW project documentation
comments: true
---

This project was born in mid-2019 to help LabVIEW developers to improve documentation provided with their projects.


### Goal

Use LabVIEW content to generate valuable documentation that will help maintain your projects and onboard new resources in the development team.

### Project Maintainer

[Olivier Jourdan](https://www.linkedin.com/in/jourdanolivier/) from [Wovalab](https://www.wovalab.com)

### Project Contributors

[Cyril Gambini](https://www.linkedin.com/in/cyrilgambini/) from [Neosoft Technologies Inc](http://www.neosoft.ca/)

[Fabiola Delacueva](https://www.linkedin.com/in/fabioladelacueva/) from [DELACOR](https://delacor.com/)

[Joerg Hampel](https://www.linkedin.com/in/joerghampel/) from [Hampel Software Engineering](https://www.hampel-soft.com/) 